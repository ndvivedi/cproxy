(ns cproxy.stats)

;globalish data, this sucks..right?
(def stats (ref {}))

(defn init-stats [endpoints]
  (dosync 
     (ref-set stats 
              (into {} (map vector endpoints (repeat (count endpoints) {:total 0}))))))

(defn record [endpoint #_event]
  (let [current (@stats endpoint)
        update (update-in current [:total] inc)]
    (dosync
      (alter stats assoc endpoint update))))

(defn stats-for-endpoint [e]
  (@stats e))
