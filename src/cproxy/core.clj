(ns cproxy.core
  (:use 
	[lamina.core]
	[aleph.http]
	[aleph.tcp]))

(defn pick-random [things]
  (let [idx (rand-int (count things))]
    (nth things idx)))

(defn host-port-string-to-map [a]
  (let [l (clojure.string/split a #":")
        host (first l)
        port (read-string (second l))]
    {:host host :port port}))

(defn make-simple-http-handler [response]
  (fn [ch r]
	(enqueue ch
	  {:status 200
	   :headers {"content-type" "text/html"}
	   :body (response r)})))

(defn make-simple-http-server [port rsp]
  (start-http-server (make-simple-http-handler rsp) {:port port}))

(defn make-client [host-port]
  (tcp-client host-port))

(defn make-handler-for-clients [endpoints]
  (fn [ch info]
    (let [endpoint (pick-random endpoints)
          client (make-client endpoint)]
	  (receive-all @client #(enqueue ch %))
      (receive-all ch #(enqueue @client %)))))

(defn make-proxy [server-port endpoints]
  (let [endpoints-map (map host-port-string-to-map endpoints)
        handler (make-handler-for-clients endpoints-map)]
    (start-tcp-server handler {:port server-port})))


