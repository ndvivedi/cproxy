(ns cproxy.test.stats
  (:use [midje.sweet])
  (:use [cproxy.stats]))

(def endpoints
  [{:host "a" :port 1}
   {:host "b" :port 1}])

(background
  (before :contents (init-stats endpoints)))

(fact "initial state"
  (stats-for-endpoint {:host "a" :port 1}) => {:total 0})

(record {:host "a" :port 1})
(fact "after one event"
  (stats-for-endpoint {:host "a" :port 1}) => {:total 1}
  (stats-for-endpoint {:host "a" :port 1}) => {:total 1})


 
