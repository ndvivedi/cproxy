(ns cproxy.test.core
  (:use [cproxy.core])
  (:use [clojure.test])
  (:use [midje.sweet])
  (:require [clj-http.client :as client]))

(defn setup-endpoints-and-proxy []
  (make-simple-http-server 8880 (fn [r] "first"))
  (make-simple-http-server 8881 (fn [r] "second"))
  (make-proxy 8889 ["localhost:8880"]))

(background
  (before :contents (setup-endpoints-and-proxy)))

(fact 
  (:body (client/get "http://localhost:8880")) => (str "first")
  (:body (client/get "http://localhost:8881")) => (str "second"))
